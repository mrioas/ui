---
title: "IPFS"
date: 2020-01-30T00:14:20-04:00
draft: false
toc: false
images:
tags:
  - electronic
  - science
---

{{<figure src="https://ipfs.io/images/ipfs-illustration-network.svg">}}
Un Protocolo peer-to-peer "punto a punto", diseñado para hacer la red más rápida, segura y más abierta

>Ref. [https://ipfs.io/](https://ipfs.io/)
