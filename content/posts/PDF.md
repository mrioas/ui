---
title: "PDF.js"
date: 2020-01-30T13:11:56-04:00
draft: false
tags: 
    - www
    - javascript
---
Es un visualizador de Portable Docuement Format (PDF) que fué construido con html5, es impulsado por la comunidad y apoyado por Mozilla Labs. La meta de esta herramienta es crear un propósito general, plataforma web basada en estándares para anáilisis y representación de archivos PDF's

### Demostración en línea

https://mozilla.github.io/pdf.js/web/viewer.html

### 