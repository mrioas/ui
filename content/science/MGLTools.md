---
title: "MGLTools"
date: 2020-01-29T20:07:52-04:00
draft: false
tags: 
    - python
    - science
    - chemistry
---

{{<figure src="http://mgltools.scripps.edu/tsri_logo.gif">}}


>Ref. [Scripps EDU](http://mgltools.scripps.edu/)

MGLTools es un software desarrollado en el Laboratorio de gráficos moleculares [Molecular Graphics Laboratory](http://mgltools.scripps.edu/) del [Instituto de Investigación Scripps](https://www.scripps.edu/) para la visualización y análisis de estructuras moleculares, una breve descripción y demostración de tres principales aplicaciones:
1. AutoDockTools (ADT).-

    Es un front-end gráfico para configurar y correr AutoDock un software de acoplamiento automatizado diseñado para predecir como las pequeñas moléculas, asi como sustratos y drogas candidatas, se unen a un receptor de estructura 3D conocida.
2. Python Molecular Viewer (PMW).-

    Es un poderoso visor molecular que tiene un número de características personalizadas y viene con varios comandos que van desde muestreo superficial molecular hasta la mayor representación de volumen.
3. Vision.-

    Es un entorno de programación visual en el cual el usuario puede crear redes interactivamente describiendo nuevas combinaciones de métodos computacionales y asi flexibilizar visualizaciones de sus datos sin escribir realmente el código.

### Instalando en Archlinux

Disponbles dos paquetes en los resitorios [AUR](https://aur.archlinux.org):
1. ```yay -S mgtools ``` instalable desde el código fuente 

```
Repository      : aur
Name            : mgltools
Keywords        : None
Version         : 1.5.6-1
Description     : Visualization and analysis of molecular structures; includes AutoDockTools, Vision and PythonMoleculeViewer
URL             : http://mgltools.scripps.edu/
AUR URL         : https://aur.archlinux.org/packages/mgltools
Groups          : None
Licenses        : custom
Provides        : None
Depends On      : swig  tk  python2-numpy  python2-imaging  python2-pmw  glut  zsi  python2-simpy  libxmu
Make Deps       : None
Check Deps      : None
Optional Deps   : autodocksuite
Conflicts With  : None
Maintainer      : mschu
Votes           : 3
Popularity      : 0.000000
First Submitted : Thu 17 Feb 2011 04:35:01 PM -04
Last Modified   : Fri 12 Jun 2015 07:07:50 AM -04
Out-of-date     : No
ID              : 193134
Package Base ID : 46605
Package Base    : mgltools
Snapshot URL    : https://aur.archlinux.org/cgit/aur.git/snapshot/mgltools.tar.gz
```
2. `yay -S mgltools-bin`

```
Repository      : aur
Name            : mgltools-bin
Keywords        : None
Version         : 2015.01.22-2
Description     : Visualization and analysis of molecular structures; includes AutoDockTools, Vision, AutoDock 4.2.6 and PythonMoleculeViewer (includes Python 2.7)
URL             : http://mgltools.scripps.edu/
AUR URL         : https://aur.archlinux.org/packages/mgltools-bin
Groups          : None
Licenses        : custom
Provides        : None
Depends On      : libpng12
Make Deps       : None
Check Deps      : None
Optional Deps   : None
Conflicts With  : None
Maintainer      : nicman23
Votes           : 1
Popularity      : 0.000214
First Submitted : Tue 04 Apr 2017 03:42:56 AM -04
Last Modified   : Sat 15 Dec 2018 06:34:37 AM -04
Out-of-date     : No
ID              : 568579
Package Base ID : 121211
Package Base    : mgltools-bin
Snapshot URL    : https://aur.archlinux.org/cgit/aur.git/snapshot/mgltools-bin.tar.gz

```