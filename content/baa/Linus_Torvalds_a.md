---
title: "Linus Torvalds a Portable Operating System"
date: 2020-02-11T22:26:42-04:00
draft: false
tags:
    - books
    - articles
    - linux
---

>Ref. [Helsinki fi Linus](https://www.cs.helsinki.fi/u/kutvonen/index_files/linus.pdf)