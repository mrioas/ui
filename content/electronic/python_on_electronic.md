---
title: "Python en la electrónica"
date: 2020-01-16
draft: false
toc: false
images:
tags: 
    - python
    - electronic
author: "Mario Aguilar"
discus_url: "https://marioaas.disqus.com"
---

Python es un lenguaje de programación multipropósito, en este articulo daremos referencias de python en el mundo de la electronica, para microprocesadores y microcontroladores programables con el lenguaje de programación python, algo que tomar en cuenta de este documento es que es introductorio y referencial hacia los proyectos activos y en desarrollo, sea open source:

### Proyectos

*   [MicroPython](http://micropython.org/)

    Es una implementación eficiente y de apoyo del lenguaje de programación [Python3](https://www.python.org/) que incluye unn pequeño subconjunto de bibliotecas estándar python y optimizado para correr en un microcontrolador y ambientes restringidos o de pocas prestaciones.
    El MicroPython [pyboard](https://store.micropython.org/pyb-features) es una compacta placa de circuito electrónico que corre MycroPython, lo suficientemente compacto para corren en 256K de espacio y 16K de RAM
    MycroPython pretende ser tan compatible con python normal como sea posible para transferir código con facilidad desde el escritorio  a un microcontrolador o sistema embebido.
    >  Caractisticas   
    >  * Soporta varias architecturas (x86, x86-64, ARM, ARM Thumb, Xtensa)
    >  * Es completamente Open Source
    >  * [Ver más documentación](http://docs.micropython.org/en/latest/)

* [Adafruit](https://learn.adafruit.com/welcome-to-circuitpython/circuitpython-libraries) con [CircuitPython](https://circuitpython.org/)

    Circuit python es un lenguaje de programación diseñado para simplicar la experimentación y aprender código en microcontroladores de bajo coste. Una de las ventajas de CircuitPython es la no necesidad de descargas en tu escritorio, una vez configurado, abre un editor de texto e inicia escribir código, asi de simple.
    > [Descargas](https://circuitpython.org/libraries) y [Documentación](https://circuitpython.org/awesome) 

* [PySpice](https://pypi.org/project/PySpice/)

    Es un programa para simular circuitos electronicos usando Python y los simuladores Ngspice/Xyce.
    La dumentación la podemos encontrarla  [aquí](https://pyspice.fabrice-salvaire.fr/)
    PySpice es un modulo gratis y Open Source que enlaza python con Ngspice y Xyce, [Ngspice](http://ngspice.sourceforge.net/) es una bifurcación del famoso simulador de circuitos [SPICE](https://en.wikipedia.org/wiki/SPICE), mientras que [Xyce](https://xyce.sandia.gov/) es un simulador compatible con SPICE desarrollado por [Sandia National Laboratories](https://www.sandia.gov/).
    PySpice implementa un enlace a Ngspice y proporciona una API orientada a objeto sobre SPICE, la salida simulación es convertido a matrices [Numpy](https://numpy.org/) por comodidad.
    > Requiere: Python 3, corre en Windows, Linux y OS X

* [Librería para Dibujar esquemas electricos en Python](https://www.collindelker.com/2014/08/29/electrical-schematic-drawing-python.html)

    Dibujar esquemas de calidad es una de esas tareas que lleva tiempo, y más si el software utilizado se enfoca en una lujosa simulación, sin tener el cuidado sobre la apariencia de diseño del circuito. pues aquí tenemos librería que como el objetivo es dibujar diagramas claros, enfocandose en que los símbolos sean entendibles, ya sea para usar en ducumentos, exámenes, libros, etc. De igual manera se puede trabajar y apuntando con Jupyter notebooks.
    Ejemplo:
    {{<figure src="https://www.collindelker.com/images/cap-charge.svg">}}
    {{<figure src="https://www.collindelker.com/images/full_add.svg">}}

    Hablamos de la libreria `SchemDraw`, las características a renombrar:
    1. Incluye de 2 a 3 terminales de componentes eléctricos: **Resistencias, Capacitor, Inductor, Transistor, Op-amps,etc.**
    2. Incluye compuerta lógicas comúnes.
    3. Fácil de agregar elementos perzonalizados.
    4. Funciones auxiliares para dibujar transformadores, así como circuitos integrados.
    5. Como es basado en python, trabaja grandiosamente con Jupyter Notebooks.
    6. Guarda imágenes en formato vector **SVG** o **EPS**, PNG, JPG, etc.

    Es de fácil instalación, en nuestro caso linux con `pip`:
    ```
    pip install SchemDraw
    ```
    ### Uso
    ```python
    import SchemDraw as schem
    import SchemDraw.elements as e
    d = schem.Drawing()
    d.add(e.RES, label='100K$\Omega$')
    d.add(e.CAP, d='down', botlabel='0.1$\mu$F')
    d.draw()
    d.save('schematic.eps')
    ```
    >Ref. [Collindelker](https://www.collindelker.com/2014/08/29/electrical-schematic-drawing-python.html)


>Ref. [Python Tutoriales](http://www.greenteapress.com/thinkpython/html/)

>Ref. [Python Exercicies](https://www.davekuhlman.org/python_book_01.pdf)
