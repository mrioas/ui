---
title: 'Humans.txt'
date: 2020-01-27
tags:
    - www
author: "Mario Aguilar"

---


> ref. [humanstxt.org](http://humanstxt.org/)

{{<figure src="http://humanstxt.org/img/logo-humans.png" title="humanstxt" alt="image no found">}}


## Qué es Humans TXT

En resumen es una iniciativa para el conocimiento de las personas que desarrollaron el website, *detrás del website*, es bastante sencillo de usar, rápido, pero lo más importante aún es que no es intrusivo con el código

## Ejemplo 

 ______  ______   __  __   ______       ______  ______   __   __       ______  __   ______   __  __    
/\  ___\/\  __ \ /\ \/\ \ /\  == \     /\__  _\/\  __ \ /\ "-.\ \     /\  ___\/\ \ /\  ___\ /\ \_\ \   
\ \  __\\ \ \/\ \\ \ \_\ \\ \  __<     \/_/\ \/\ \ \/\ \\ \ \-.  \    \ \  __\\ \ \\ \___  \\ \  __ \  
 \ \_\   \ \_____\\ \_____\\ \_\ \_\      \ \_\ \ \_____\\ \_\\"\_\    \ \_\   \ \_\\/\_____\\ \_\ \_\ 
  \/_/    \/_____/ \/_____/ \/_/ /_/       \/_/  \/_____/ \/_/ \/_/     \/_/    \/_/ \/_____/ \/_/\/_/ 
                                                                                                       
/* humanstxt.org */

/* TEAM */
/* WELL, JUST ME, REALLY */

Developer, Designer, Website Administrator & Pretty Much Everything Else: Stefan Bohacek

  Email: stefan@fourtonfish.com
  Twitter: https://twitter.com/fourtonfish
  GitHub: https://github.com/fourtonfish
  Flickr: http://www.flickr.com/photos/fourtonfish/
  My Blog: http://blog.fourtonfish.com/

/* SITE */

Language: English
Powered by: HTML5, CSS3, JavaScript, Python and Flask (some projects also use PHP, MySQL, jQuery, AngularJS etc)
IDE: Sublime Text
Hosted on: DigitalOcean

/* ATTRIBUTIONS */

* my wife & son
* Chris, my very first beta tester: http://cjwworld.cu.cc/

> Ref. [https://fourtonfish.com/humans.txt](https://fourtonfish.com/humans.txt)