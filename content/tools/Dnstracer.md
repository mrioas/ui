---
title: "Dnstracer"
date: 2020-02-14T11:48:41-04:00
draft: false
tags: 
    - software
    - linux
    - dns

---


Dnstracer **es una herramienta de recopilación de información del servidor de nombres de dominio, que extrae información DNS única sobre un dominio. Extrae diferentes tipos de registros DNS como NS, MX, A, AAAA, SOA, NSEC, etc.** Determina de dónde un servidor de nombres de dominio obtiene su información.


### Tipos de registro DNS

Si bien el total de registros DNS asciende a algo más de 25 registros, normalmente sólo se utilizan unos pocos en cualquier configuración estándar de una zona DNS, como por ejemplo para un servicio de correo y alojamiento.

Éstos son algunos de los registros más utilizados:

* **A** (Address) – Es el registro más utilizado. Este define a que dirección IP debe resolver un nombre de dominio concreto.
    dominio.com    A    xxx.xxx.xxx.xxx
* **PTR** (Pointer) – También conocido como registro inverso o `rDNS`, funciona a la inversa del registro A, traduciendo IP’s en nombres de dominio, y la usan diversas aplicaciones para comprobación de identidad del cliente.
* **MX** (Mail eXchanger) – Se usa para identificar servidores de correo, se pueden definir uno o más servidores de correo para un dominio, pudiendo definir la prioridad de cada registro.

    
        dominio.com     |   MX(10)    |     mail.dominio.com

* **CNAME** (Canonical Name) – Es un alias que se asigna a un host que tiene una dirección IP válida, y que responde a diversos nombres. Pueden declararse varios para un mismo dominio. Es importante saber que un registro CNAME sólo puede apuntar a un tipo registro A existente, y no debería apuntar nunca a otro CNAME pues podría crear loops infinitos.

        www.dominio.com   | CNAME  |  dominio.com

* **NS** (Name Server) – Define los servidores de nombres principales de un dominio. Debe haber al menos uno, pero pueden declararse varios para un dominio.

        dominio.com  |  NS |  ns1.dominio.com
        dominio.com  |  NS |  ns2.dominio.com

* **SOA** (Start Of Authority) – Este es el primer registro de la zona, sólo puede haber uno configurado, y sólo está presente si el servidor es autoritario del dominio. Especifica el servidor DNS primario del dominio, la cuenta de correo del administrador y tiempo de refresco de los servidores secundarios. Comunmente este puntero es definido por el proveedor de hosting donde se aloja la zona DNS del dominio.

* **TXT** (Text) – Permite asociar información adicional a un dominio. Esto se utiliza para otros fines, como el almacenamiento de claves de cifrado. En la actualidad el uso más extendido es para configurar el SPF de un dominio.

>Nota, Existen algunas restricciones en el uso de los registros CNAME que podrían provocar que toda la zona DNS del dominio dejará de funcionar. Por ejemplo, si configura un registro CNAME con el mismo nombre que otro registro de la zona, o un registro CNAME en el root de la zona.

* Ejemplos:
    Zona DNS

        sub.dominio.com		CNAME	dominio.com
        sub.dominio.com		MX(10)	mail.dominio.com

 
    Zona DNS

        dominio.com		CNAME	sub.dominio.com



Según especialistas en hacking ético del Instituto Internacional de Seguridad Cibernética, DNStracer ayuda a encontrar las diversas consultas de DNS que se pueden usar en ataques de fuerza bruta y otras actividades de hacking.

Para usar DNStracer, escriba dnstracer en el terminal de Linux como se muestra a continuación.

#### Escaneo predeterminado

Teclee 
```
$ dnstracer -v -o mario.blog.bo
```
Por defecto enviará una consulta de DNS para los registros A.
- **v** modo verboso, para mostrar solicitudes y respuestas que van y vienen
- **a** muestra el resumen de la exploración del dominio
- **o** Permitir una visión general de las respuestas recibidas

#### Uso de consulta SOA

Teclee 
```
$ dnstracer -q soa -o -4 mario.blog.bo
```

- **q** significa tipo de registro DNS (aquí el tipo de registro DNS es SOA)
- **o** imprimir el resumen en la consola
- **4** significa ignorar IPv6

Muestra los registros SOA (estado de autoridad). Los registros SOA guardan la información sobre el servidor de nombres que proporcionó datos para la zona.

#### Utilizar la consulta NS (servidor de nombres)

Teclee 
```
dnstracer -q ns -o -4 zonetransfer.me
```
- **q** significa tipo de registro DNS (aquí el tipo de registro DNS es NS)
- **o** imprimir el resumen en la consola
- **4** significa ignorar IPv6


### Instalar 

* Archlinux, disponible en el repositorio `community`
    ```
    $ yay -S dnstracer
    ```

>Ref. [**EN** Mavetju dnstracer ](http://www.mavetju.org/unix/dnstracer.php)
>
>Ref. [**ES** Noticias Seguridad dnstracer ](https://noticiasseguridad.com/reconnaissance/enumeracion-dns-usando-dnstracer/)