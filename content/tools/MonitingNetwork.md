---
title: "Monitoreando tu red"
date: 2020-02-09T22:06:35-04:00
draft: false
---

### Monitoreando Velocidad y Ancho de Banda Linux

Hoy en dia la interconectividad mundial es de hecho una de las herramientas más importantes en los negocios, comunicación social, hobbies, en fin con un gran tráfico de millones de Bytes ya sea de manera local o en internet, es por eso de hecho que existen herramientas para verificar cual es la velocidad de nuestro internet, pero también del ancho de banda, claro tomando en cuenta parámetros como el tipo de hardware y conexión que va incidir en el resultado

#### Tipo de Hardware 

##### Por Cable

Las de redes de cables también han evolucionado, como la manera en la cual se transmite la información, pero siempre con el fundamento de base binario a nivel lógico(**0**, **1**) ó en palabras no técnicas hay o no hay corriente, los medios que serán nombrados en orden cronológicos:

1. **Par trenzado**, que como la palabra lo indica son dos cables de material trenzados entre sí con el propósito de cancelar la [interferencia electromagnética]() (EMI) por sus siglas en inglés, es 


### Herramientas de monitoreo en Linux

#### iftop
Ref.-[iftop info ex-parrot](http://www.ex-parrot.com/~pdw/iftop/)

Es una herramienta de monitoreo hace uso de la red, escuchando así el tráfico de una indicada interfaz, ordena y muestra en tiempo real el ancho de banda de cada paquete transmitido, el total, RX total y TX total
* Requerimientos.- 
    * [libpcap](https://www.tcpdump.org/), poderoso analizador de paquetes por linea de comandos
    * libcurses